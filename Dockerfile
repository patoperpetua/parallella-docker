#
# This Dockerfile provides a reproducible development environment for
# developing applications for the Adapteva Parallella development board.
#
# See: http://www.parallella.org
#

FROM ubuntu:16.04

LABEL mainteiner="Patricio Perpetua <patricio.perpetua.arg@gmail.com>" \
    name="singletonar/parallella-dev" \
    architecture="x86_64" \
    vendor="UNC - FCEFYN - LAC" \
    vcs-type="git" \
    vcs-url="https://gitlab.com/patoperpe/parallella-docker.git" \
    distribution-scope="public" \
    Summary="Image to program and compile binaries for Parallella Board."

# Install prerequisites.
RUN apt-get update -qq && apt-get install -y \
    build-essential \
    bison \
    flex \
    g++-arm-linux-gnueabihf \
    gcc-arm-linux-gnueabihf \
    git \
    libgmp3-dev \
    libncurses5-dev \
    libmpc-dev \
    libmpfr-dev \
    locate \
    python \
    texinfo \
    wget \
    xzip \
    lzip \
    zip \
    && rm -rf /var/lib/apt/lists/*

# Setup a new user 'dev' and add to sudoers.
RUN adduser --quiet --shell /bin/bash --gecos "Epiphany Developer,101,," --disabled-password dev && \
    adduser dev sudo && \
    chown -R dev:dev /home/dev/ && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Create the sdk folder and workspace.
RUN mkdir -p /opt/adapteva && \
    chown -R dev:dev /opt/adapteva && \
    mkdir -p /home/dev/workspace && \
    chown -R dev:dev /home/dev/workspace
    
# Set up new user and home directory in environment.
USER dev
ENV HOME /home/dev

# Download the compiled sdk and setup it.
# Remove temporary files to save space.
WORKDIR /opt/adapteva
RUN wget --no-check-certificate https://github.com/adapteva/epiphany-sdk/releases/download/esdk-2016.11/esdk.2016.11.x86_64.tar.gz && \
    tar -xvzf esdk.2016.11.x86_64.tar.gz && \
    rm esdk.2016.11.x86_64.tar.gz && \
    ln -sTf /opt/adapteva/esdk.2016.11 /opt/adapteva/esdk

# Set environment variables for the new toolchain.
ENV EPIPHANY_HOME /opt/adapteva/esdk
ENV PATH ${EPIPHANY_HOME}/tools/e-gnu/bin:${EPIPHANY_HOME}/tools/host/bin:${PATH}
ENV LD_LIBRARY_PATH ${EPIPHANY_HOME}/tools/host/lib:${LD_LIBRARY_PATH}
ENV EPIPHANY_HDF ${EPIPHANY_HOME}/bsps/current/platform.hdf
ENV MANPATH ${EPIPHANY_HOME}/tools/e-gnu/share/man:${MANPATH}

RUN echo 'source ${EPIPHANY_HOME}/setup.sh' >> ~/.bashrc
# Download the official Epiphany examples repository into $HOME/examples.
WORKDIR /home/dev/
RUN wget --no-check-certificate https://github.com/adapteva/epiphany-examples/archive/master.zip && \
    unzip master.zip && \
    rm master.zip && \
    mv epiphany-examples-master examples

# Start at $HOME.
WORKDIR /home/dev

# Expose a port so that GDB can connect to a Parallella board.
EXPOSE 51000

#Expose a folder to share with the host.
VOLUME /home/dev/workspace

# Start from a BASH shell.
CMD ["bash"]
