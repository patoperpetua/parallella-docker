#!/usr/bin/env bash

#Exit when a command fails.
set -o errexit
#Exit when script tries to use undeclared variables.
set -o nounset
#The exit status of the last command that threw a non-zero exit code is returned.
set -o pipefail

#Trace what gets executed. Useful for debugging.
#set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)"

#Check if docker is installed.
if ! type "docker" > /dev/null; then
    echo -ne "Docker is not installed. Do you want to install it? [Y/n]: "
    read RESPONSE
    if [ "$RESPONSE" = "" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "Y" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "Yes" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "yes" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "YES" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "y" ]; then
        source Docker.sh
    else
        exit 1
    fi
fi

if [ "$#" -eq "1" ]; then
    VERSION=$1
fi

#If version was not suplied by arguments
if [ -z ${VERSION+x} ]; then 
    VERSION="$(cat VERSION)"
fi

#Getting current datetime.
DATE="$(date --rfc-3339=ns | sed 's/ /T/; s/\(\....\).*-/\1-/g')"

#Get last commited hash. 
GIT_COMMIT_HASH=$(git log --pretty=format:'%h' -n 1 2> /dev/null || true )
echo Version: ${VERSION}
echo Commit Hash: ${GIT_COMMIT_HASH}
echo Date: ${DATE}
if [ ! -z "${GIT_COMMIT_HASH}" ] ; then
    docker build --rm -f Dockerfile \
    -t singletonar/parallella-dev:${VERSION} \
    --label "version=${VERSION}"  \
    --label "vcs-ref=$GIT_COMMIT_HASH" \
    --label "build-date=${DATE}" .
else
    docker build --rm -f Dockerfile \
    -t singletonar/parallella-dev:${VERSION} \
    --label "version=${VERSION}"  \
    --label "build-date=${DATE}" .
fi