[![pipeline status](https://gitlab.com/patoperpe/parallella-docker/badges/master/pipeline.svg)](https://gitlab.com/patoperpe/parallella-docker/commits/master)

parallella-devenv  [![Docker repo](http://img.shields.io/badge/docker-repo-blue.svg)](https://hub.docker.com/r/singletonar/parallella-dev/)
=================

Development environment for the [Adapteva Parallella board](http://www.parallella.org/).


Installing and running
----------------------

Start by [installing docker](https://docs.docker.com/installation/#installation) for your platform. There is un script available here called Docker.sh to install docker on a computer. 

The rest of this guide is based on a Ubuntu environment, but any other environment should be similar.

This container is based on a Ubuntu 16.04 image. You can start the container by executing run.sh and providing a folder to attach the workspace folder of the container where code will be written:

```bash
$ ./run.sh -p workspace
```

```bash
$ ./run.sh -p /home/my/computer/full/path/workspace
```
This will create a BASH shell in the `$HOME` directory which is located at `/home/dev/`.

There are two directories you need to know about:

  * `/opt/adapteva/esdk` - which contains an installed version of the Epiphany SDK and toolchain. Relevant environment variables (such as `$PATH`, `$LD_LIBRARY_PATH` and `$MANPATH`) have been set. The path `/opt/adapteva/` is readable and writeable by the default user `dev`.
  * `/home/dev/examples` - a clone of the official [Epiphany examples](https://github.com/adapteva/epiphany-examples) repository.

![demo](https://raw.githubusercontent.com/futurecore/parallella-devenv/master/screenshots/demo-start.gif)

Compiling code for the Parallella board
------------------------------------------------------

To compile and simulate the `hello-world` example from the [Epiphany examples](https://github.com/adapteva/epiphany-examples) repository, first change directory:

```bash
$ cd ~/examples/apps/hello-world
```

then run the build script to with:

```bash
$ ./build.sh
```

This will build all the relevant binary files and place them in the `Debug` directory.

![demo](https://raw.githubusercontent.com/futurecore/parallella-devenv/master/screenshots/demo-compile.gif)

Contributing
------------

Contributions to this repository are very welcome.

To contribute, please fork this repository on GitHub and send a pull request with a clear description of your changes. If appropriate, please ensure that the user documentation in this README is updated.

If you have submitted a PR and not received any feedback for a while, feel free to [ping me on Twitter](https://twitter.com/patoperpetua)


TODO
----
Change demo screenshots.
Put a link to download only the run script and docker script.
---------------------------------------

© Perpetua Patrico, Universidad Nacional de Cordoba, Argentina, 2018.
