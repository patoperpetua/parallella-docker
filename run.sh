#!/usr/bin/env bash

#Exit when a command fails.
set -o errexit
#Exit when script tries to use undeclared variables.
set -o nounset
#The exit status of the last command that threw a non-zero exit code is returned.
set -o pipefail

#Trace what gets executed. Useful for debugging.
#set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)"
RUN_VERSION=1.0.0
#Check if docker is installed.
if ! type "docker" > /dev/null; then
    echo -ne "Docker is not installed. Do you want to install it? [Y/n]: "
    read RESPONSE
    if [ "$RESPONSE" = "" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "Y" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "Yes" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "yes" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "YES" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "y" ]; then
        source Docker.sh
    else
        exit 1
    fi
fi

#If version was not suplied by arguments
if [ -z ${VERSION+x} ]; then 
    VERSION="$(cat VERSION)"
fi

function usage {
  echo "    DOCKER PARALLELLA RUNNER - v${RUN_VERSION}"
  echo "        -h,    --[Hh]elp          help."
  echo "        -[Vv], --[Vv]ersion       version."
  echo "        -[Pp], --[Pp]ath       main folder where files will be located."
  echo "Repository: https://gitlab.com/patoperpe/parallella-docker.git"
  exit 0
}

if [ "$#" -eq "0" ]; then
  echo -e "No option was provided."
  echo -e "$(usage)"
  exit 1
fi
TEMP=`getopt -o [Hh]:[Vv]:[Pp]: --long [Vv]ersion,[Hh]elp,[Pp]ath`
HELP=" Available"
while true ; do
  case $1 in
    -[Vv] | --[Vv]ersion)
      echo -e "$VERSION"
      exit 0
      ;;
    -[Hh] | --[Hh]elp)
      echo -e "$(usage)"
      exit 0
      ;;
    -[Pp] | --[Pp]ath)
        if [ "$#" -eq 2 ]; then
            #If absolute path was provided.
            if [[ $2 == /* ]]; then
                PATH_WORKDIR=$2
            else
                PATH_WORKDIR=${__dir}/${2}
            fi
            #Create folder if it does not exits.
            if [ ! -d ${PATH_WORKDIR} ]; then
                mkdir ${PATH_WORKDIR}
            fi
        else
            echo "Error: Must provide the path as an argument."
            exit 1 
        fi
      break
      ;;
    *)
      echo -e "Invalid option: $1"
      echo -e "$(usage)"
      exit 1
      ;;
  esac
done

#Checking if there is a container is alredy created.
CONTAINERS="$(docker ps -a | grep singletonar/parallella-dev | awk 'NF>1{print $NF}' || true)"
# Save current IFS
SAVEIFS=$IFS
# Change IFS to new line. 
IFS=$'\n'
CONTAINERS=($CONTAINERS)
# Restore IFS
IFS=$SAVEIFS

#Check available ports to bind.
DEFAULT_PORT=51000
while [ ! -z "$(netstat -lntu | grep ${DEFAULT_PORT})" ]; do
    let DEFAULT_PORT=DEFAULT_PORT+1 
done

#If there is not a container.
if [ "$CONTAINERS" = "" ]; then
    docker run \
        -v ${PATH_WORKDIR}:/home/dev/workspace \
        --rm -it -p ${DEFAULT_PORT}:51000 --name parallella-dev \
        patricioperpetua/parallella-dev:${VERSION}    
else
    while [ -z "${opt:-}" ]; do
        PS3='Contenedores encontrados. Seleccione una opcion: '
        options=("${CONTAINERS[@]}" "quit" "new container" "erase all containers")
        select opt in "${options[@]}"
        do
            if [ -z "$opt" ]; then 
                echo "Opción invalida."
                continue
            fi
            if [ "$opt" = "quit" ]; then 
                exit 0
            fi
            break;
        done
        break;
    done
    if [ "$opt" = "new container" ]; then
        docker run \
            -v ${PATH_WORKDIR}:/home/dev/workspace \
            --rm -it -p ${DEFAULT_PORT}:51000 --name parallella-dev${#CONTAINERS[@]} \
            patricioperpetua/parallella-dev:${VERSION}
        exit
    fi
    echo "Contenedor seleccionado: ${opt}"
    #Show docker container info
    docker inspect ${opt} --format='{{json .Config.Labels}}' | python -m json.tool
    docker exec -it ${opt} /bin/bash
fi

#TODO: versionar este archivo. obtener la version del nombre.