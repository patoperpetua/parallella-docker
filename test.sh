#!/usr/bin/env bash

#Exit when a command fails.
set -o errexit
#Exit when script tries to use undeclared variables.
set -o nounset
#The exit status of the last command that threw a non-zero exit code is returned.
set -o pipefail

#Trace what gets executed. Useful for debugging.
#set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)"

#Check if docker is installed.
if ! type "docker" > /dev/null; then
    echo -ne "Docker is not installed. Do you want to install it? [Y/n]: "
        exit 1
    fi
fi

#If version was not suplied by arguments
if [ -z ${VERSION+x} ]; then 
    VERSION="$(cat VERSION)"
fi

function usage {
  echo "    DOCKER PARALLELLA RUNNER - v${VERSION}"
  echo "        -h,    --[Hh]elp          help."
  echo "        -[Vv], --[Vv]ersion       version."
  echo "        -[Pp], --[Pp]ath       main folder where files will be located."
  echo "Repository: https://gitlab.com/patoperpe/parallella-docker.git"
  exit 0
}

if [ "$#" -eq "0" ]; then
  echo -e "No option was provided."
  echo -e "$(usage)"
  exit 1
fi
TEMP=`getopt -o [Hh]:[Vv]:[Pp]: --long [Vv]ersion,[Hh]elp,[Pp]ath`
HELP=" Available"
while true ; do
  case $1 in
    -[Vv] | --[Vv]ersion)
      echo -e "$VERSION"
      exit 0
      ;;
    -[Hh] | --[Hh]elp)
      echo -e "$(usage)"
      exit 0
      ;;
    -[Pp] | --[Pp]ath)
        if [ "$#" -eq 2 ]; then
            #If absolute path was provided.
            if [[ $2 == /* ]]; then
                PATH_WORKDIR=$2
            else
                PATH_WORKDIR=${__dir}/${2}
            fi
            #Create folder if it does not exits.
            if [ ! -d ${PATH_WORKDIR} ]; then
                mkdir ${PATH_WORKDIR}
            fi
        else
            echo "Error: Must provide the path as an argument."
            exit 1 
        fi
      break
      ;;
    *)
      echo -e "Invalid option: $1"
      echo -e "$(usage)"
      exit 1
      ;;
  esac
done

#Checking if there is a container is alredy created.
CONTAINERS="$(docker ps -a | grep singletonar/parallella-dev | awk 'NF>1{print $NF}' || true)"

#If there is not a container.
if [ "$CONTAINERS" = "" ]; then
    response $(docker run \
        -v ${PATH_WORKDIR}:/home/dev/workspace \
        --rm -it -p 51000:51000 --name parallella-dev \
        patricioperpetua/parallella-dev:${VERSION})    
else
    echo "Contenedores encontrados: ${CONTAINERS}"
    #TODO: buscar la forma de seleccionar un contenedor que ya ha sido creado.
    #TODO: mostrar opcion para borrar contendor y mostrar los labels.
fi